#!/usr/bin/env bash

function printer_question ()
{
	eval $3
	while true; do

		echo "$1" >&2
		read -p "$2" answer
		VALUE=${choices[$answer]}

		if [ -z "$VALUE" ];
		then

			echo "Neplatná odpověď " >&2
			echo "" >&2
		else
			echo $VALUE & break
		fi
	done
	echo "" >&2
}

function run () {
	local PROJECT_NAME=${1}
	local OUTPUT_PARAMS=""
	local OUTPUT_CMD=""
	local IS_CMD=0

	for param in "${@:2}"; do
		if [[ "${param}" ==  *"="* ]]; then param="${param/=/=\"}\""; else param="${param}"; fi
		if [[ $IS_CMD != 1 ]]; then if [[ ${param} != -* ]]; then IS_CMD=1; fi; fi
        if [[ $IS_CMD == 1 ]]; then  OUTPUT_CMD="${OUTPUT_CMD} ${param}"; else OUTPUT_PARAMS="${OUTPUT_PARAMS} ${param}"; fi
	done

	if [ ! -f "/tmp${DF__PROJECT_PATH}/${PROJECT_NAME}/docker-compose.yml" ]; then
	    installProject ${PROJECT_NAME}
	fi

	eval docker-compose -f /tmp${DF__PROJECT_PATH}/${PROJECT_NAME}/docker-compose.yml run --rm ${OUTPUT_PARAMS} env ${OUTPUT_CMD} >&2
}

function checkProjectName {
	if [[ "${1}" == "" ]]; then
		echo " FAIL: Second params (project name) must be set."
		exit;
	fi

	if [[ ! -d $(echo "${DF__PROJECT_PATH}/${1}") ]]; then
		echo " FAIL: Project does not exist. Add project into projects directory."
		exit;
	fi
}

function printProjectList {
	DIR=${1}
	if [ "$(ls -A $DIR)" ]; then
        for i in $(ls -d ${DIR}*); do echo " - ${i/$DIR/}"; done
	else
	    echo "   There is no projects"
	fi
}

function stopProject {
	checkProjectName ${1}

	PROJECT_NAME=${1}

	run ${PROJECT_NAME} docker-compose -f /environment/${PROJECT_NAME}/docker-compose.yml stop
}

function startProject {
	checkProjectName ${1}

	PROJECT_NAME=${1}

	run ${PROJECT_NAME} docker-compose -f /environment/${PROJECT_NAME}/docker-compose.yml start
}

function installProject {

	PROJECT_NAME=${1}

	#check insert project name
	checkProjectName ${PROJECT_NAME}

	WORK_DIR=/tmp${DF__PROJECT_PATH}/${PROJECT_NAME}
	mkdir -p ${WORK_DIR}

	#vytvorim volume directory
	mkdir -p /ds/volumes/${PROJECT_NAME}

	#do template
eval "cat > ${WORK_DIR}/docker-compose.yml << EOF
$(cat /ds/docker-compose-project-template.yml)
EOF"

	#run entry point
	run ${PROJECT_NAME}

}

function uninstallProject {

	PROJECT_NAME=${1}

	#check insert project name
	checkProjectName ${PROJECT_NAME}

	RESULT=$(printer_question "Uninstall project $PROJECT_NAME?" "Yes[Y] / No[n]: " "declare -A choices=( ['Y']='true' ['n']='false' )" )
	if [ $RESULT != true ]; then
		exit 1;
	fi

	echo "- stopping and remove project containers"
	run ${PROJECT_NAME} docker-compose -f /environment/${PROJECT_NAME}/docker-compose.yml down

	#smazu scripts directory
	echo "- remove project scripts"
	rm -rf /ds/scripts/${PROJECT_NAME}

	#smazu volume directory
	echo ""
	RESULT=$(printer_question "Delete project volumes (all project data will be lost)?" "Yes[Delete] / No[n]: " "declare -A choices=( ['Delete']='true' ['n']='false' )" )
	if [ $RESULT == true ]; then
		rm -rf /ds/volumes/${PROJECT_NAME}
	fi
}

if [[ "${1}" == "ls" ]]; then

	echo " Linked project:"
	printProjectList "$DF__PROJECT_PATH/"

	echo ""
	echo " Installed project:"
	printProjectList "$DF__SCRIPT_PATH/"

	exit;
fi

if [[ "${1}" == "run" ]]; then
	checkProjectName ${2}
	PARAMS=""; for var in "${@:3}"; do if [[ "${var}" ==  *"="* ]]; then VAR="${var/=/=\"}\""; else VAR="${var}"; fi; PARAMS="${PARAMS}  $VAR"; done
	eval run ${2} ${PARAMS}
	exit;
fi

if [[ "${1}" == "exec" ]]; then
	checkProjectName ${2}

	PROJECT_NAME=${2}
	SERVICE_NAME=${3}
	PARAMS=${@:4}

	if [ ! -f "/tmp${DF__PROJECT_PATH}/${PROJECT_NAME}/docker-compose.yml" ]; then
	    installProject ${PROJECT_NAME}
	fi

	eval run ${2} docker-compose -f /environment/${PROJECT_NAME}/docker-compose.yml exec ${SERVICE_NAME} ${PARAMS} >&2
fi

if [[ "${1}" == "install" ]]; then
	if [[ "${2}" == "--all" ]]; then
		DIR=${DF__PROJECT_PATH}/
		if [ "$(ls -A $DIR)" ]; then
	        for i in $(ls -d ${DIR}*); do installProject ${i/$DIR/}; done
		else
		    echo "   There is no projects"
		fi
	else
		installProject ${2}
	fi
	exit;
fi

if [[ "${1}" == "uninstall" ]]; then
	if [[ "${2}" == "--all" ]]; then
		DIR=${DF__PROJECT_PATH}/
		if [ "$(ls -A $DIR)" ]; then
	        for i in $(ls -d ${DIR}*); do uninstallProject ${i/$DIR/}; done
		else
		    echo "   There is no projects"
		fi
	else
		uninstallProject ${2}
	fi
	exit;
fi

if [[ "${1}" == "start" ]]; then
	if [[ "${2}" == "--all" ]]; then
		DIR=${DF__SCRIPT_PATH}/
		if [ "$(ls -A $DIR)" ]; then
	        for i in $(ls -d ${DIR}*); do startProject ${i/$DIR/}; done
		else
		    echo "   There is no projects"
		fi
	else
		startProject ${2}
	fi
	exit;
fi

if [[ "${1}" == "stop" ]]; then
	if [[ "${2}" == "--all" ]]; then
		DIR=${DF__SCRIPT_PATH}/
		if [ "$(ls -A $DIR)" ]; then
	        for i in $(ls -d ${DIR}*); do stopProject ${i/$DIR/}; done
		else
		    echo "   There is no projects"
		fi
	else
		stopProject ${2}
	fi

	exit;
fi

# HELP SECTION
echo " DOCKER-SERVER - help:"
echo " ====================="
echo " Syntax: project <command>"
echo " Commands:"
echo "  - ls                                        List all projects"
echo "  - run <projectName> [\"<params>\"][<cmd>]   Run a command"
echo "  - exec <projectName> <serviceName> [<cmd>]  Execute a command for specific service"
echo "  - install <projectName>                     Run entrypoint script in environment (commonly install"
echo "                                              scripts for control project throw environment)"
echo "  - uninstall <projectName>                   Uninstall project"
echo "  - start <projectName>                       Star project"
echo "  - stop  <projectName>                       Stop stop project"