# DOCKER SERVER

Docker-server is a tool how to simple managed multiple web services with different hosts.

####Requirements
- docker
- docker-compose

For more information about **docker** or **docker-compose** visit official pages:
- https://docs.docker.com/engine/installation/
- https://docs.docker.com/compose/install/


#### Download run scripts
```
curl -L https://bitbucket.org/jn-dev/ds/raw/master/run/ds.sh >> ./ds.sh
curl -L https://bitbucket.org/jn-dev/ds/raw/master/run/docker-compose.yml >> ./docker-compose.yml
```

### Set docker engine
Make sure, that your docker engine has allowed make volumes in to the docker-server direcotry

#### Create bin
For access ``ds`` command from anywhere make simlink to the bin directory.

	cd /usr/local/bin
    ln -s /path/to/the/ds.sh ds

Restart terminal and thats it. Try in command line execute command ``ds``

You should see somethink like this:

	 DOCKER-SERVER - help:
	 =====================
	 Syntax: dockerServer.sh <command>
	
	 Commands:
	  - example                  Build an example image and run it on domain example.io
	    example clean            Remove example build
	  - exec '<cmd>'             Execute a command
	  - help                     Show this help
	  - install                  Install DOCKER-SERVER
	  - start                    Star DOCKER-SERVER
	  - stop                     Stop DOCKER-SERVER
	  - uninstall                Uninstall DOCKER-SERVER

TIP: If you have a problem and ``which ds`` not working check permissions on dockerServer.sh - must be execute


##TODO: 
- make scripts downloadable from repository
- web application
	- for control deployment on server
	- for statistic purpose