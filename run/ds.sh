#!/usr/bin/env bash

function install () {

	docker pull jndev/ds:alpine
	docker pull jndev/ds-env:alpine

	docker network create ds
	docker-compose -f ${DOCKER_COMPOSE_TARGET} pull
	docker-compose -f ${DOCKER_COMPOSE_TARGET} up -d
	docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server bash install.sh
}

function uninstall () {
	docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server bash uninstall.sh
	docker-compose -f ${DOCKER_COMPOSE_TARGET} down --volume
	docker network rm ds
}

##GET THE DOCKER FILE DOCKER-SERVER PATH (FROM DS COMMAND)
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  TARGET="$(readlink "$SOURCE")"
  if [[ $TARGET == /* ]]; then
#    echo "SOURCE '$SOURCE' is an absolute symlink to '$TARGET'"
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
#    echo "SOURCE '$SOURCE' is a relative symlink to '$TARGET' (relative to '$DIR')"
    SOURCE="$DIR/$TARGET" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  fi
done
#echo "SOURCE is '$SOURCE'"
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
#echo "DIR is '$DIR'"

DOCKER_COMPOSE_TARGET=$DIR/docker-compose.yml
DOCKER_COMPOSE_PATH=$DIR
PROJECT_NAME=${DIR##*/}

cd ${DOCKER_COMPOSE_PATH}

echo ""
########################################################################################################################
#
#    CHECK REQUIRED
#
########################################################################################################################
hash docker-compose &> /dev/null
if [ $? -eq 1 ]; then
	echo "DOCKER-SERVER ERROR: docker-compose not found!"
	echo ""
	exit;
fi

########################################################################################################################
#
#    INSTALL
#
########################################################################################################################
if [[ "$1" == "install" ]]; then
	install;
	echo ""; exit;
fi

########################################################################################################################
#
#    UPDATE
#
########################################################################################################################
if [[ "$1" == "update" ]]; then

	uninstall
	install
	echo ""; exit;
fi

########################################################################################################################
#
#    UNINSTALL
#
########################################################################################################################
if [[ "$1" == "uninstall" ]]; then
	uninstall
	echo ""; exit;
fi

########################################################################################################################
#
#    STOP
#
########################################################################################################################
if [[ "$1" == "stop" ]]; then
	#TODO ZASTAVIT VSECHNY PROJEKTY

	docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server bash stop.sh
	docker-compose -f ${DOCKER_COMPOSE_TARGET} stop
	echo ""; exit;
fi

########################################################################################################################
#
#    START
#
########################################################################################################################
if [[ "$1" == "start" ]]; then
	docker-compose -f ${DOCKER_COMPOSE_TARGET} start
	docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server bash start.sh

	#TODO ROZJET VSECHNY PROJEKTY
	echo ""; exit;
fi

########################################################################################################################
#
#    EXEC
#
########################################################################################################################
if [[ "$1" == "exec" ]]; then

	if [[ "$2" == "" ]]; then
		echo " FAIL: Second params must be set. Example \"exec 'docker stats'\"."
		echo ""
		exit;
	fi

	docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server ${@:2}
	echo ""
	exit;
fi

########################################################################################################################
#
#    STATUS
#
########################################################################################################################
if [[ "$1" == "status" ]]; then
	docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server docker ps -a
	echo ""
	exit;
fi

########################################################################################################################
#
#    STATS
#
########################################################################################################################
if [[ "$1" == "stats" ]]; then
	docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server docker stats
	echo ""
	exit;
fi

########################################################################################################################
#
#    PROJECT
#
########################################################################################################################
if [[ "$1" == "project" ]]; then
	PARAMS=""; for var in "${@:2}"; do if [[ "${var}" ==  *"="* ]]; then VAR="${var/=/=\"}\""; else VAR="${var}"; fi; PARAMS="${PARAMS}  $VAR"; done


#	docker-compose -f /Users/jirka/work/composetest/ds/available_projects/primat/docker-compose.yml

#exit;
#echo $(pwd)
#exit;

	eval docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server bash /ds/project-manager.sh ${PARAMS}

#	eval docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server docker-compose -f /projects/primat2/docker-compose.yml run --rm -d -e PROJECT_NAME=primat2 env
	echo ""
	exit;
fi

########################################################################################################################
#
#
#    EXAMPLE
#
########################################################################################################################
if [[ "$1" == "example" ]]; then

	CONTAINER_NAME="${PROJECT_NAME}_example_test"
	if [[ "$2" == "clean" ]]; then
		docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server docker rm -f $CONTAINER_NAME >/dev/null
		docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server docker rmi -f tutum/hello-world:latest >/dev/null

		echo " SUCCESS: Example was removed."
		echo ""
		exit;
	fi

	docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server docker rm -f $CONTAINER_NAME >/dev/null
	docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server docker run --name $CONTAINER_NAME -d -e VIRTUAL_HOST=example.io --network=ds tutum/hello-world:latest

	echo ""
	echo " SUCCESS: Visit example.io in browser. Maybe you need add line '127.0.0.1  example.io' into hosts."
	echo ""

	exit;
fi



########################################################################################################################
#
#    PROJECT
#
########################################################################################################################
if [[ "$1" == "backup" ]]; then
	PARAMS=""; for var in "${@:2}"; do if [[ "${var}" ==  *"="* ]]; then VAR="${var/=/=\"}\""; else VAR="${var}"; fi; PARAMS="${PARAMS}  $VAR"; done
	eval docker-compose -f ${DOCKER_COMPOSE_TARGET} exec server bash /ds/backup.sh ${PARAMS}
	echo ""
	exit;
fi

########################################################################################################################
#
#    CLEAN
#
########################################################################################################################
if [[ "$1" == "clean" ]]; then
	echo "Start cleaning exited <project_name>_env_run_<nr>"
	docker ps -a | grep _env_run_ | grep Exited | awk {'print $1'} | xargs docker rm
	echo "cleaned"
	echo ""
	exit;
fi


########################################################################################################################
#
#    HELP
#
########################################################################################################################
echo " docker-server - help:"
echo " ====================="
echo " Syntax: ds <command>"
echo " Commands:"
echo "  - example                  Build an example image and run it on domain example.io"
echo "    example clean            Remove example build"
echo "  - exec '<cmd>'             Execute a command"
echo "  - help                     Show this help"
echo "  - stats                    Stats docker-server"
echo "  - status                   Status docker-server"
echo "  - project                  Project Management"
echo "  - backup                   Backup Management"
echo "  - clean                    Clean exited project env containers"
echo "  -----------------------------------------------------------------------------------"
echo "  - install                  Install docker-server"
echo "  - start                    Star docker-server"
echo "  - stop                     Stop docker-server"
echo "  - update                   Update docker-server"
echo "  - uninstall                Uninstall docker-server"
echo ""