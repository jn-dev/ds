#!/usr/bin/env bash

if [[ "${1}" == "set" ]]; then

	rm /ds/settings/crontab.set
	echo "${2} ${3} ${4} ${5} ${6} bash /ds/backup.sh run" >> /ds/settings/crontab.set

	crontab /ds/settings/crontab.set

	echo "Automatic backup was set."

	exit;

fi

if [[ "${1}" == "unset" ]]; then

	crontab crontab.unset

	crontab -l >> /ds/settings/crontab.set

	echo "Automatic backup was unset"
	exit;

fi

if [[ "${1}" == "info" ]]; then

	echo "Automatic backup is set: "

	crontab -l

	exit;
fi

if [[ "${1}" == "run" ]]; then

	BACKUP_DESTINATION=/ds/backup
	SCRIPTS=/ds/scripts/

	YEAR=$(date +"%Y")
	MONTH=$(date +"%m")
	DAY=$(date +"%d")

	BACKUP_LOG_DIR=${BACKUP_DESTINATION}/$YEAR/$MONTH/$DAY
	BACKUP_LOG=${BACKUP_LOG_DIR}/backup-$(date +"%Y-%m-%d_%H.%M.%S").log


	#Kontrola log directory
	if [ ! -d "$BACKUP_LOG_DIR" ]; then
		mkdir -p ${BACKUP_LOG_DIR}
	fi

	if [ ! -d "$BACKUP_DESTINATION" ]; then
		log_now "! There is no backup directory in docker-compose.yml"
		exit
	fi

	log_now()  {
	  echo "`date +%Y/%m/%d\ %H:%M` $@"
	}

	function backup {

		PROJECT_NAME=${1}
		BACKUP_PROJECT_DESTINATION=${BACKUP_DESTINATION}/${PROJECT_NAME}
		BACKUP_SCRIPT=${SCRIPTS}${PROJECT_NAME}/backup

		echo ""
		log_now
		echo "----------------"
		echo "Backup project: " ${PROJECT_NAME}
		echo "--------------------------------------------------------------------"

		if [ ! -f "$BACKUP_SCRIPT" ]; then
			echo "- !!! There is no backup script ($BACKUP_SCRIPT)"
		else
			bash $(log_now $(cat  $BACKUP_SCRIPT) | sed 's/.* project/\/ds\/project-manager\.sh/g')
		fi
		echo "--------------------------------------------------------------------"
		log_now
		echo ""
	}

	# presmerovani do logu
	exec 1>>${BACKUP_LOG} 2>&1

	echo ""
	echo "===================================================================="
	echo "="
	echo "=   RUN COMPLETE BACKUP ($(date +"%Y-%m-%d_%H.%M.%S"))"
	echo "="
	echo "===================================================================="
	echo "" >> ${BACKUP_LOG}

	if [ "$(ls -A $SCRIPTS)" ]; then
	    for i in $(ls -d ${SCRIPTS}*); do backup ${i/$SCRIPTS/}; done
	else
	    echo "   There is no projects"
	fi

	echo ""
	echo "===================================================================="
	echo "="
	echo "=   BACKUP FINISH  ($(date +"%Y-%m-%d_%H.%M.%S"))"
	echo "="
	echo "===================================================================="
	log_now

	if [ ! -z ${MAILGUN_API_KEY+x} ] && [ ! -z ${MAILGUN_DOMAIN+x} ] && [ ! -z ${MAILGUN_FROM+x} ] && [ ! -z ${MAILGUN_TO+x} ]; then

		subject="Backup $(date +"%Y-%m-%d_%H.%M.%S")"
		html="<div width='100%'><h1>Backup ($(date +"%Y-%m-%d_%H.%M.%S"))</h1><hr><div style='border: 1px dashed; margin: 20px; padding: 20px;'>"
		url="https://api.mailgun.net/v3/$MAILGUN_DOMAIN/messages"

		while read -r line
		do
		 html="$html <br>$line"
		done <"${BACKUP_LOG}"

		rm /mail.tmp;
		echo "$html </div></div>" >> /mail.tmp
		echo -ne "sending...\n"

		req="curl --user 'api:${MAILGUN_API_KEY}' '$url' -F from='${MAILGUN_FROM}' -F to='${MAILGUN_TO}' -F subject='${subject}'  -F html='</mail.tmp' -k"
		eval $req
	fi

	exit;

fi

########################################################################################################################
#
#    HELP
#
########################################################################################################################
echo " docker-server - help:"
echo " ====================="
echo " Syntax: ds backup <command>"
echo " Commands:"
echo "  - set <crontab-rule>     Set crontab rule for automatic backup( ds backup 0 22 \\\* \\\* \\\* ) - every day in 22pm start backup"
echo "                             crontab-rule ( Minute Hour DayOfMonth Month DayOfWeek]"
echo "  - unset                    Stop automatic backup"
echo "  - run                      Run backup"
echo ""