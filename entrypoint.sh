#!/usr/bin/env bash

# pokud crontab set neco obsahuje nastavim podle toho, pokud ne, nacpu do nej aktualni konfiguraci
if [[ -s /ds/settings/crontab.set ]]; then
	crontab /ds/settings/crontab.set
else
	crontab -l >> /ds/settings/crontab.set
fi

#nahodi se cron
crond -f