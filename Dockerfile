FROM alpine

RUN apk update && \
	apk add --no-cache -t .deps bash openssh docker py-pip tzdata curl &&  \
	rm -rf /var/cache/apk/* && \
	pip install docker-compose

#EXPOSE 80
#EXPOSE 443

#Nastaveni timezone
RUN cp /usr/share/zoneinfo/Europe/Prague /etc/localtime

RUN mkdir -p /ds/projects

ADD ./docker-compose.yml /ds/docker-compose.yml
ADD ./docker-compose-project-template.yml /ds/docker-compose-project-template.yml

ADD ./entrypoint.sh /entrypoint.sh
ADD ./install.sh /ds/install.sh
ADD ./project-manager.sh /ds/project-manager.sh
ADD ./start.sh /ds/start.sh
ADD ./stop.sh /ds/stop.sh
ADD ./backup.sh /ds/backup.sh
ADD ./uninstall.sh /ds/uninstall.sh

ENV DF__SCRIPT_PATH=/ds/scripts
ENV DF__PROJECT_PATH=/ds/projects

WORKDIR /ds

RUN echo "#Automatic backup is not set" >> /ds/crontab.unset

RUN crontab /ds/crontab.unset

#pridat nejkaej hezci nekonecnej process sleep print project inside... nebo neco podobneho - muze byt jen otevrena otazka :)
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]